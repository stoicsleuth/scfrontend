import React from 'react';
import './App.css';

import POrtfolio from './components/Portfolio';

function App() {
  return (
    <div className="App">
      <POrtfolio/>
    </div>
  );
}

export default App;
