import React, { Component } from "react";
import { NavLink, Link} from "react-router-dom";

export default class Navbar extends Component {
  render() {
    return (
      <nav>
        <ul>
          <li className="links">
            <span>FrontEnd</span>
            <NavLink
              to="/portfolio"
              activeStyle={{
                color: "orange"
              }}
            >
              Portfolio
            </NavLink>
            <NavLink
              activeStyle={{
                color: "orange"
              }}
              to="/transact"
            >
              Transact
            </NavLink>
          </li>

          <li> <a href="https://gitlab.com/stoicsleuth/scbackend" style={{ textDecoration: 'none', color: 'inherit'}}>GitLab </a>  </li>
        </ul>
      </nav>
    );
  }
}
