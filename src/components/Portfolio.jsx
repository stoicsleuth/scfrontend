import React, { Component } from "react";
import axios from "axios";

export default class Portfolio extends Component {
  state = {
    holding: null,
    returns: null,
    portfolio: null,
    isHolding: false,
    isPortfolio: false,
    isReturns: false
  };

  handleHolding = () => {
    axios
      .get("https://whispering-wave-74413.herokuapp.com/api/fetch/holding")
      .then(response => response.data)
      .then(data =>
        this.setState({
          holding: data,
          isHolding: true,
          isPortfolio: false,
          isReturns: false
        })
      );
  };

  handlePortfolio = () => {
    axios
      .get("https://whispering-wave-74413.herokuapp.com/api/fetch/portfolio")
      .then(response => response.data)
      .then(data =>
        this.setState({
          portfolio: data,
          isHolding: false,
          isPortfolio: true,
          isReturns: false
        })
      );
  };

  handleReturns = () => {
    axios
      .get("https://whispering-wave-74413.herokuapp.com/api/fetch/returns")
      .then(response => response.data)
      .then(data =>
        this.setState({
          returns: data,
          isHolding: false,
          isPortfolio: false,
          isReturns: true
        })
      );
  };

  render() {
    const {
      returns,
      portfolio,
      holding,
      isHolding,
      isPortfolio,
      isReturns
    } = this.state;
    return (
      <div>
        <div className="button-group">
          <button onClick={this.handleHolding}>Holding</button>
          <button onClick={this.handlePortfolio}>Portfolio</button>
          <button onClick={this.handleReturns}>Returns</button>
        </div>
        {isHolding && (
          <div className="holdings">
            {holding.map(holding => {
              if (holding.quantity > 0) {
                return (
                  <div key={holding.ticker} className="holding">
                    <span className="holding-price">₹{holding.avg_price}</span>

                    <div className="holding-info">
                      <span>{holding.ticker}</span>
                      <span>{holding.quantity}</span>
                    </div>
                  </div>
                );
              }
            })}
          </div>
        )}
        {isReturns && (
          <div className="returns">
            {returns.map(return_one => (
              <div key={1} className="holding">
                <span className="holding-price">₹{return_one.total}</span>

                <div className="holding-info">
                  <span>Total Returns</span>
                </div>
              </div>
            ))}
          </div>
        )}
        {isPortfolio && (
          <div className="portfolios">
            {portfolio.map(portfolio => {
              if (portfolio.transactions.length>0)
                return (
                  <div key={portfolio.ticker} className="portfolio">
                    <h2>{portfolio.ticker}</h2>
                    <table>
                      <thead>
                        <tr>
                          <th>Transaction ID</th>
                          <th>Quantity</th>
                          <th>Type</th>
                          <th>Price</th>
                        </tr>
                      </thead>
                      <tbody>
                        {portfolio.transactions.map(transaction => (
                          <tr key={transaction.t_id}>
                            <td>{transaction.t_id}</td>
                            <td>{transaction.quantity}</td>
                            <td>{transaction.t_type.toUpperCase()}</td>
                            <td>{transaction.price}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                );
            })}
          </div>
        )}
      </div>
    );
  }
}
