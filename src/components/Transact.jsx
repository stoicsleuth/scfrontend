import React, { Component } from "react";
import axios from "axios";

export default class Transact extends Component {
  state = {
    isAdd: null,
    isUpdate: null,
    isDelete: null,
    t_id: "",
    price: "",
    quantity: "",
    t_type: "BUY",
    ticker: "",
    message: ""
  };
  handleChange = e => {
    let change = {};
    change[e.target.name] = e.target.value.trim();
    this.setState(change);
  };

  handleSubmitDelete = e => {
    e.preventDefault();
    const { t_id } = this.state;
    let transaction = {
      t_id
    };
    axios
      .post(
        `https://whispering-wave-74413.herokuapp.com/api/order/delete`,
        transaction
      )
      .then(res => {
        this.setState({
          isAdd: false,
          isDelete: false,
          isUpdate: false,
          message: `Transaction ${t_id} is deleted successfuly. 🔫`
        });
      })
      .catch(err => {
        
        this.setState({
          isAdd: false,
          isDelete: false,
          isUpdate: false,
          message: `${err} 🔫`
        });
      });
  };

  handleSubmitUpdate = e => {
    e.preventDefault();
    const { t_id, quantity, price } = this.state;
    let transaction = {
      t_id,
      quantity,
      price
    };
    axios
      .post(
        `https://whispering-wave-74413.herokuapp.com/api/order/update`,
        transaction
      )
      .then(res => {
        this.setState({
          isAdd: false,
          isDelete: false,
          isUpdate: false,
          message: `Transaction ${t_id} is updated successfuly. 🔥`
        });
      })
      .catch(err => {
        err = err.response.data.errors[0].msg;
        this.setState({
          isAdd: false,
          isDelete: false,
          isUpdate: false,
          message: `${err} 🔫`
        });
      });
  };
  handleSubmit = e => {
    e.preventDefault();
    const { t_type, ticker, quantity, price } = this.state;
    let transaction = {
      t_type,
      ticker,
      quantity,
      price
    };
    axios
      .post(
        `https://whispering-wave-74413.herokuapp.com/api/order/add`,
        transaction
      )
      .then(res => {
        this.setState({
          isAdd: false,
          isDelete: false,
          isUpdate: false,
          message: `Transaction ${res.data.t_id} is placed successfuly. 🔥`
        });
      })
      .catch(err => {
        if (err.response.data.errors[0].msg)
          err = err.response.data.errors[0].msg;
        this.setState({
          isAdd: false,
          isDelete: false,
          isUpdate: false,
          message: `${err} 🔫`
        });
      });
  };
  handleAdd = () => {
    this.setState({
      isAdd: true,
      isDelete: false,
      isUpdate: false,
      message: null
    });
  };
  handleUpdate = () => {
    this.setState({
      isAdd: false,
      isDelete: false,
      isUpdate: true,
      message: null
    });
  };
  handleDelete = () => {
    this.setState({
      isAdd: false,
      isDelete: true,
      isUpdate: false,
      message: null
    });
  };
  render() {
    const { isAdd, isDelete, isUpdate, message } = this.state;
    return (
      <div>
        <div className="button-group">
          <button onClick={this.handleAdd}>Add Transaction</button>
          <button onClick={this.handleUpdate}>Update Transaction</button>
          <button onClick={this.handleDelete}>Delete Transaction</button>
        </div>
        {message && <h2 align="center">{message}</h2>}
        {isAdd && (
          <form className="addForm" onSubmit={this.handleSubmit}>
            <input
              name="ticker"
              type="text"
              value={this.state.ticker}
              placeholder="Ticker"
              onChange={this.handleChange}
              required
            />

            <input
              name="quantity"
              type="number"
              placeholder="Quantity"
              value={this.state.quantity}
              onChange={this.handleChange}
              required
            />

            <input
              name="price"
              type="number"
              placeholder="Price"
              value={this.state.price}
              onChange={this.handleChange}
              required
            />

            <select
              name="t_type"
              type="text"
              placeholder="Type"
              value={this.state.t_type}
              onChange={this.handleChange}
              required
            >
              <option defaultValue value="BUY">
                BUY
              </option>
              <option value="SELL">SELL</option>
            </select>

            <input type="submit" value="Submit" />
          </form>
        )}
        {isUpdate && (
          <form className="addForm" onSubmit={this.handleSubmitUpdate}>
            <input
              name="t_id"
              type="text"
              value={this.state.t_id}
              placeholder="Transaction ID"
              onChange={this.handleChange}
              required
            />

            <input
              name="quantity"
              type="number"
              placeholder="Quantity"
              value={this.state.quantity}
              onChange={this.handleChange}
              required
            />

            <input
              name="price"
              type="number"
              placeholder="Price"
              value={this.state.price}
              onChange={this.handleChange}
              required
            />
            <input type="submit" value="Submit" />
          </form>
        )}
        {isDelete && (
          <form className="addForm" onSubmit={this.handleSubmitDelete}>
            <input
              name="t_id"
              type="text"
              placeholder="Transaction ID"
              value={this.state.t_id}
              onChange={this.handleChange}
              required
            />
            <input type="submit" value="Submit" />
          </form>
        )}
      </div>
    );
  }
}
